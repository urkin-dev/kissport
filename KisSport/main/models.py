from django.conf import settings
from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    birthday = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.user.username


class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    productName = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    cost = models.CharField(max_length=64)

    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.productName


class Attribute(models.Model):
    value = models.CharField(max_length=255)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return self.value
