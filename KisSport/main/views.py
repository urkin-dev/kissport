import jwt
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import exceptions, status, viewsets
from rest_framework.decorators import api_view, permission_classes
from .models import Product, Category
from .serializers import UserBaseSerializer, UserUpdateSerializer, ProductSerializer, CategorySerializer
from .utils import generate_access_token, generate_refresh_token
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated


class UserView(APIView):
    @staticmethod
    def get(request):
        users = User.objects.all()
        serializer = UserBaseSerializer(users, many=True)
        return Response(serializer.data)

    @staticmethod
    def post(request):
        serializer = UserBaseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        if serializer.errors:
            return Response(serializer.errors, status=status.HTTP_409_CONFLICT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def put(request):
        user_id = request.data.get('id')
        user = User.objects.filter(id=user_id).first()

        if user is None:
            return Response("User doesn't exist", status.HTTP_404_NOT_FOUND)

        serializer = UserUpdateSerializer(instance=user, data=request.data)
        if serializer.is_valid():
            user = serializer.update(instance=request.data, validated_data=serializer.data)
            user = UserUpdateSerializer(user).data
            return Response(user)
        else:
            return Response(serializer.errors, status.HTTP_409_CONFLICT)

    @staticmethod
    def delete(request):
        user_id = request.data.get('id')
        password = request.data.get('password')
        user = User.objects.filter(id=user_id).first()

        if not user.check_password(password):
            return Response('Wrong password or username', status.HTTP_401_UNAUTHORIZED)
        else:
            user = user.delete()
            return Response(user, status=status.HTTP_200_OK)

    def get_permissions(self):
        if self.request.method == 'GET':
            self.permission_classes = [IsAuthenticated]
        elif self.request.method == 'PUT':
            self.permission_classes = [IsAuthenticated]
        elif self.request.method == 'DELETE':
            self.permission_classes = [IsAuthenticated]
        else:
            self.permission_classes = []

        return super(UserView, self).get_permissions()


@permission_classes([AllowAny])
class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


@permission_classes([AllowAny])
class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


@api_view(['POST'])
@permission_classes([AllowAny])
def login(request):
    username = request.data.get('username')
    email = request.data.get('email')
    password = request.data.get('password')
    response = Response()

    if (username is None) or (password is None):
        return Response('Wrong password or username', status.HTTP_401_UNAUTHORIZED)

    user = User.objects.filter(username=username, email=email).first()

    if user is None or not user.check_password(password):
        return Response('Wrong credentials', status.HTTP_401_UNAUTHORIZED)

    serialized_user = UserBaseSerializer(user).data

    access_token = generate_access_token(user)
    refreshed_token = generate_refresh_token(user)

    response.set_cookie(key='refreshtoken', value=refreshed_token, httponly=True)

    response.data = {
        'accessToken': access_token,
        'user': serialized_user,
    }

    return response


@api_view(['POST'])
@permission_classes([AllowAny])
def logout(request):
    response = Response()
    response.delete_cookie('refreshtoken')

    return response


@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token(request):
    refreshed_token = request.COOKIES.get('refreshtoken')
    if refreshed_token is None:
        raise exceptions.AuthenticationFailed(
            'Authentication credentials were not provided.')
    try:
        payload = jwt.decode(
            refreshed_token, settings.REFRESH_TOKEN_SECRET, algorithms=['HS256'])
    except jwt.ExpiredSignatureError:
        raise exceptions.AuthenticationFailed(
            'expired refresh token, please login again.')

    user = User.objects.filter(id=payload.get('user_id')).first()
    if user is None:
        raise exceptions.AuthenticationFailed('User not found')

    if not user.is_active:
        raise exceptions.AuthenticationFailed('user is inactive')

    access_token = generate_access_token(user)
    return Response({'accessToken': access_token})


@api_view(['GET'])
def get_user(request, user_id):
    response = Response()
    if user_id is None:
        raise exceptions.AuthenticationFailed('UserID is required')

    user = User.objects.get(id=user_id)

    if user is None:
        raise exceptions.AuthenticationFailed('User not found')

    serialized_user = UserBaseSerializer(user).data

    response.data = {
        'user': serialized_user,
    }

    return response
