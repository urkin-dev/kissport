from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import *

router = DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'categories', ProductViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('login', login, name="login"),
    path('logout', logout, name="logout"),
    path('users/', UserView.as_view()),
    path('users/<int:user_id>', get_user),
    path('refresh-token', refresh_token, name='refresh-token'),
]
