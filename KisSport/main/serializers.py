from .models import *
from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rest_framework import pagination
from rest_framework.response import Response


class CustomPagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        return Response({
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'count': self.page.paginator.count,
            'totalPages': self.page.paginator.num_pages,
            'results': data,
            'page': self.page.number,
            'limit': self.page_size
        })


class ProfileSerializer(serializers.ModelSerializer):
    birthday = serializers.DateField(required=False)

    class Meta:
        model = Profile
        fields = ['birthday']


class UserBaseSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(required=False)
    firstName = serializers.CharField(required=False, source="first_name")
    lastName = serializers.CharField(required=False, source="last_name")
    password = serializers.CharField(
        write_only=True,
        required=True,
        help_text='Leave empty if no change needed',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )

    class Meta:
        model = User
        fields = ['id', 'username', 'firstName', 'lastName', 'email', 'password', 'profile']

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            password=make_password(validated_data['password']),
        )

        if "profile" in validated_data and validated_data['profile'] is not None:
            profile_data = dict(validated_data['profile'])
            birthday = None

            if 'birthday' in profile_data:
                birthday = profile_data['birthday']

            Profile.objects.create(
                user=user,
                birthday=birthday
            )

        return user


class UserUpdateSerializer(UserBaseSerializer):
    password = serializers.CharField(
        write_only=True,
        required=False,
        help_text='Leave empty if no change needed',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )

    class Meta:
        model = User
        fields = ['id', 'username', 'firstName', 'lastName', 'email', 'password', 'profile']

    def update(self, instance, validated_data):
        user = User.objects.get(id=instance.get('id'))
        user.username = instance.get('username')
        user.email = instance.get('email')

        if instance.get('firstName') is not None:
            user.first_name = instance.get('firstName')

        if instance.get('lastName') is not None:
            user.last_name = instance.get('lastName')
        user.save()

        profile = user.profile

        if instance.get('birthday'):
            profile.birthday = instance.get('birthday')

        profile.save()
        return user


class ProductSerializer(serializers.ModelSerializer):
    category = serializers.SlugRelatedField(slug_field='name', queryset=Category.objects.all())

    class Meta:
        model = Product
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class AttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attribute
        fields = '__all__'
