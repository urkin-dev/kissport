from django.contrib import admin

from .models import *


class ProfileAdmin(admin.ModelAdmin):
    fields = ['user', 'birthday']


class CategoryAdmin(admin.ModelAdmin):
    fields = ['name', 'description']


class AttributeAdmin(admin.ModelAdmin):
    fields = ['value', 'product']


class ProductAdmin(admin.ModelAdmin):
    fields = ['productName', 'description', 'cost', 'category']


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Attribute, AttributeAdmin)
admin.site.register(Product, ProductAdmin)
